var mysql = require('mysql');
var _ = require('underscore');
var pool  = mysql.createPool({
    host     : 'localhost',
    database : 'dong9app',
    user     : 'node',
    password : '!ehdrnqkx0223',
    multipleStatements : true
});


function executeQuery(sql, callback) {
    pool.getConnection(function(err, connection) {
        connection.query(sql, function(err, rows, fields) {
            if (err) { callback && callback(err); }
            else{
              //  console.log('[DB]: ', sql+"\n >>> ", rows);
                callback && callback(rows);
            }
        });
        connection.release();   
    });
}

function executeQueryValue(sql, vlaue, callback) {
    pool.getConnection(function(err, connection) {
        connection.query(sql, vlaue, function(err, rows, fields) {
            if (err) { callback && callback(err); }
            else{
              //  console.log('[DB]: ', sql+"\n >>> ", rows);
                callback && callback(rows);
            }
        });
        connection.release();   
    });
}

function parseWhereClause(query){
    var where = [];
    _.each( query, function(value, key){
        where.push([key,"'"+value+"'"].join("="));
    });

    if( where.length > 0){
        where = ' WHERE ' + where.join("&&");
    }
    return where;
}

module.exports = {
    test : function(callback){
        executeQuery("SHOW TABLES", callback);
    },
    insertQuery : function(sql, callback) {
        executeQueryValue("INSERT INTO " + sql.table + " SET ?", sql.value, callback);
    },
    selectQuery : function(query, callback){
        var where = parseWhereClause(query.where);

        executeQuery('SELECT * FROM '+ query.table + where, callback);
    },

    updateQuery : function(query, callback){
        var where = parseWhereClause(query.where);

        executeQueryValue('UPDATE '+ query.table +' SET ? ' + where, query.value, callback);
    }
};