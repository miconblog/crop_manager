var express = require('express'), 
    app = express(),
    db = require('./db');
  

app.use(express.bodyParser());

app.get("/api/collect/uuid", function(req, res){
    db.test(function(result){
        res.json(result);
    });
});

app.post("/api/collect/uuid", function(req, res){
    console.log("BODY ", req.body);

    var values = req.body;
    var now = new Date();

    db.selectQuery({
        table: "uuid_collect",
        where: {
            uuid : values.uuid
        }
    }, function(rows){

        console.log("SELECT", rows);
        if( rows.length > 0){

            console.log("UPDATE!!!");
             db.updateQuery({
                table: "uuid_collect",
                value: {
                    last_connected_at : now,
                    count : values.count
                },
                where: {
                    uuid : values.uuid
                }
            }, function(result){
                res.json({
                    success : "OK",
                    message : "UPDATE"
                });
            });

        }else{

            console.log("INSERT!!!");
            values.create_at = now;
            values.last_connected_at = now;
            db.insertQuery({
                table : "uuid_collect",
                value : values
            }, function(result){
                
                res.json({
                    success : "OK",
                    message : "INSERT"
                }); 
                
            });
        }
    });

});

app.listen(8000);
console.log("Server Strat! Listen 8000");
