global.config = require('./package');
var express = require('express'),
	app = express(),
	crop = require('./routes/crops'),
	service = require('./routes/service'),
	selector = require('./routes/imageSelector');
	
// Global Configuration
app.configure(function(){
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');

	// app.use(function(req, res, next) {
	// 	console.log(">>>>", req.url);
	// 	// if(/\.(jpg|png)/.exec(req.url) {
	//  //    	res.setHeader("Cache-Control", "max-age=31556926");
	//  //    }
	//     return next();
	// });

	app.use(express.bodyParser()); 
	//app.use(express.logger());	
	app.use(express.static(__dirname + '/public'));
});

// Routes Configure
service.setRouter(app);

app.get('/crops', crop.findAll);
app.get('/crops/:id', crop.findById);
app.post('/crops', crop.addCrop);
app.put('/crops/:id', crop.updateCrop);
app.delete('/crops/:id', crop.deleteCrop);
app.get('/:i18n/popup/imageSelector/:type', selector.findAll);

// Application 
app.listen(config.app.port);
console.log('Listening on port ' + config.app.port);

