/* --------------------------------------------------------------------------------
 * db.js 테스트
 * - getAllCropInfo: 작물 리스트 조회
 * - insertCropInfo: 작물 정보 추가
 * - updateCropInfo: 작물 정보 수정
 * - deleteCropInfo: 작물 정보 삭제
 * - getCropInfo: 작물 정보 조회 (tb_missions 테이블을 조인하여 mission 정보도 가져온다.)
 * --------------------------------------------------------------------------------
 */

var db = require('./db');

var cropInfo = {
    name: '신규',
    ordering: 99,
    period: '100',
    difficulty: 5,
    graph: '[6]',
    icon: 'crops/user.png',
    img: 'cropImage/test.png'
}

setTimeout(function(){

	//* 작물 정보 추가
	db.insertCropInfo(cropInfo, function(req, res) {
		console.log("* 작물 정보 추가 완료: insertCropInfo");
		printCrops(); // 전체 작물 출력
	});
	//*/
        
}, 1000)

setTimeout(function(){

	//* 작물 정보 수정
	cropInfo.cropId = 21;
	cropInfo.name = '신규수정';
	cropInfo.ordering = 88;
	db.updateCropInfo(cropInfo, function(req, res) {
		console.log("* 작물 정보 수정 완료: updateCropInfo");
		printCrops(); // 전체 작물 출력
	});
	//*/
        
}, 2000)

setTimeout(function(){

	//* 작물 정보 삭제
	db.deleteCropInfo('21', function(req, res) {
		console.log("* 작물 정보 삭제 완료: deleteCropInfo");
		printCrops();
	});
	//*/
        
}, 3000)

function printCrops() {
	// 작물 리스트 조회
	db.getAllCropInfo(function(data){
		for (i = 0; i < data.length; i++) {
			var cropInfo = data[i];
		
			console.log(cropInfo.id + "\t" +
						cropInfo.name + "\t" +
						cropInfo.image);
		}
	});
}