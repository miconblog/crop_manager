var sqlite3 = require('sqlite3').verbose(),
	db = new sqlite3.Database('dong9.sqlite'),
	APP_ROOT = "http://dong9.org/app",
	_ = require('underscore');

/**
 * 작물 리스트 조회
 * id, name, image
 */
exports.getAllCropInfo = function(callback){

	var result = [];

	db.each("SELECT * FROM tb_crops ORDER BY ordering", function(err, row) {
		var info = {
			id : row.cropId,
			name : row.name,
			image : row.icon
		};

		// console.log("ROW: ", row);

		if(row.cropId > 0){
			result.push(info);	
		}
		
	}, function(){
		return callback(result);
	});
}

/* --------------------------------------------------------------------------------
 * 작물 정보 관련 (tb_crops)
 * - insertCropInfo: 작물 정보 추가
 * - updateCropInfo: 작물 정보 수정
 * - deleteCropInfo: 작물 정보 삭제
 * - getCropInfo: 작물 정보 조회 (tb_missions 테이블을 조인하여 mission 정보도 가져온다.)
 * --------------------------------------------------------------------------------
 */

/**
 * 작물 정보 추가
 */
exports.insertCropInfo = function(cropInfo, callback){

	var query =
		"insert into tb_crops(cropId, name, ordering, period, difficulty, graph, icon, img) " +
		"values ( " +
		"		  ( select max(cropId) + 1 from tb_crops )," +
		"		  $name, " +
		"         $ordering, " +
		"         $period, " +
		"         $difficulty, " +
		"         $graph, " +
		"         $icon, " +
		"         $img " +
		"       ) ";
  
	db.run( 
		query,
		{
			$name : cropInfo.name,
			$ordering : cropInfo.ordering,
			$period : cropInfo.period,
			$difficulty : cropInfo.difficulty,
			$graph : cropInfo.graph,
			$icon : cropInfo.icon,
			$img : cropInfo.img
		}, function(){
			return callback();
	});
}

/**
 * 작물 정보 수정: cropId 에 해당하는 작물 정보를 update 한다.
 */
exports.updateCropInfo = function(cropInfo, callback) {
	
	var query = 
		"update tb_crops " +
		"   set name = $name, " +
		"    	ordering = $ordering, " +
		"	    period = $period, " +
		"	    difficulty = $difficulty, " +
		" 	    graph = $graph, " +
		"	    icon = $icon, " +
		"	    img = $img " +
		" where cropId = $cropId ";
		
	db.run(
		query,
		{
			$name : cropInfo.name,
			$ordering : cropInfo.ordering,
			$period : cropInfo.period,
			$difficulty : cropInfo.difficulty,
			$graph : JSON.stringify(cropInfo.graph),
			$icon : cropInfo.icon,
			$img : cropInfo.img,
			$cropId : cropInfo.cropId
		}, function(){
			return callback();
	});
}

/**
 * 작물 정보 삭제: cropId 에 해당하는 작물 정보를 delete 한다.
 */
exports.deleteCropInfo = function(cropId, callback) {
	
	var query = 
		"delete from tb_crops " +
		" where cropId = $cropId ";
		
	db.run(
		query,
		{
			$cropId : cropId
		}, function(){
			return callback();
	});
}

/**
 * 작물 정보 조회: 작물을 선택하면, 해당하는 정보가 모두 출력 된다.(작물 정보 + 미션 정보)
 */
exports.getCropInfo = function(id, callback){

	db.get("SELECT * FROM tb_crops WHERE cropId=$id", {$id:id}, function(err, row){
		var result = _.clone(row);
		var missions = [];

		//result.img = APP_ROOT + '/image/' + row.img;
		result.graph = JSON.parse(row.graph);

		var query = 
			"SELECT DISTINCT A.cropId, B.icon, B.day, B.title, B.guide "+
			"FROM tb_crops AS A INNER JOIN tb_missions AS B ON A.cropId = B.cropId "+
			"WHERE A.cropId=$id";

	
		db.each(query,{$id:id},  function(err, row){
			console.log("MISSION: ", row);
			
			//row.icon = ICONS[row.icon -1];
			//row.guide = APP_ROOT + '/guide/' + row.guide + ".html"
			missions.push(row);
				
		}, function(){
			return callback(result, missions);
		});
	});
}

/* --------------------------------------------------------------------------------
 * 미션 관련 (tb_missions)
 * - updateMissions: 미션 정보 수정 (미션은 추가/수정/삭제 시 모두 이 메소드를 사용한다.)
 * --------------------------------------------------------------------------------
 */

/**
 * TODO: 미션 수정
 * 미션 수정 시 내부적으로 delete / insert 함.
 * 비동기로 수행되므로 callback 에서 처리하도록 해야 함.
 */
exports.updateMissions = function(cropId, missions, callback) {

	var deleteQuery = 
		"delete tb_missions " +
		" where cropId = $cropId ";
	
	var insertQuery = 
		"insert into tb_missions ( cropId, title, day, icon, guide ) " +
		"values ( $cropId, $title, $day, $icon, $guide ) ";
	
	// delete 의 callback 에서 insert
	db.run(
		deleteQuery,
		{
			$cropId : cropId
		}
	);
	
	// missions 에서 mission 을 가져와서 insert 함.	
	for (i = 0; i < missions.length; i++) {
		var mission = missions[i];
		
		db.run(
			insertQuery,
			{
				$cropId : cropId,
				$title : mission.title,
				$day : mission.day,
				$icon : mission.icon,
				$guide : mission.guide
			}
		);
	}
	
	// delete, insert 는 1 Tran 에 묶여야 한다.
}