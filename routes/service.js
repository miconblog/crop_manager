var db = require('../db');

function prepare(req, res) {

    var method = req.route.method.toUpperCase(), params=null, query=null;

    switch(method){
        case "PUT":
        case "GET":
        case "DELETE":
            params = req.params;
            break;

        case "POST":
            params = req.body;
            break;
    }

    console.log("[Request]", method, req.url, params, req.query);
    this(req, res);
}

function main(req, res) {
    res.render("manage_crop/main", {});
}

function create(req, res){
    res.render("new_crop",{});
}

exports.setRouter = function(app){
    app.get('/', prepare.bind(main));
    app.get('/crop/new', prepare.bind(create));
};