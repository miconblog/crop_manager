var db = require('../db');

// POST - create
exports.addCrop = function(req, res){
	res.render('new_crop', null);		
};

// GET - read
exports.findAll = function(req, res){
	db.getAllCropInfo(function(data){
		res.render('manage_crop/manage', {
            version : config.version,
            crops: data
        });	
	});
};

// GET - read
exports.findById = function(req, res) {
	db.getCropInfo(req.params.id, function(cropInfo, missions){
		
		res.render('manage_crop/edit', {
            version : config.version,
			crop: cropInfo, 
			missions: missions
		});
	});	
};	

// PUT - update
exports.updateCrop = function(req, res) {
    var id = req.params.id;
    var crop = req.body;

    console.log('Updating crop: ' + id);
    console.log('body: ', req.body);
    
    db.updateCropInfo(crop, function(){
    
        res.json({"success" : "updated"})

    });

}


// DELETE - delete
exports.deleteCrop = function(req, res) {
    var id = req.params.id;
    console.log('Deleting wine: ' + id);
    db.collection('wines', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}