var fs = require("fs"),
	SERVER_HOME = config.app.local, 
	URL_HOME = "http://dong9.org/app",
	VERSION = config.version;

function getFileList(i18n, type){
	var path = [SERVER_HOME, VERSION], dirs, list = [];

	path.push("images/" + type);
	dirs = fs.readdirSync(path.join("/"));			
	dirs.forEach(function(dir){
	    if (dir.indexOf(".") !== 0) {
	    	if(dir.indexOf("@2x") < 0){
	    		list.push( {
	    			path : [URL_HOME, VERSION, "images", type, dir].join("/"),
	    			link : [type, dir].join("/")
	    		});	
	    	}
	    }
	});
	console.log("dir.. > ", list);
	return list;
}

exports.findAll =  function(req, res){

	var files = getFileList(req.params.i18n, req.params.type);

	res.render('imageSelector', {type:req.params.type, files: files});	
};