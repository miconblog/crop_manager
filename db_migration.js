var sqlite3 = require('sqlite3').verbose();
var oldDb = new sqlite3.Database('dong9org.sqlite');
var newDb = new sqlite3.Database('dong9.sqlite');

// 임시
function create() {

    /*
    drop table tb_missions;

	create table tb_missions (
    	cropId integer not null,
    	title text not null,
    	day text not null,
    	icon text,
    	guide text
 	);
    */

	var query = 
		"drop table tb_missions";
	
	newDb.run(
		query
	);	

	// create tb_missions
    query = 
		"create table tb_missions ( " +
        "       cropId integer not null, " +
        "       title text not null, " +
        "       day text not null, " +
        "       icon text, " +
        "       guide text " +
 		")";
		
	newDb.run(
		query
	);
}


// 모두 삭제
function init() {
	
	var query = 
		"delete from tb_crops";
		
	newDb.run(
		query
	);
	
	query = "delete from tb_missions";
	
	newDb.run(
		query
	);
	
	query = "delete from tb_icons";
	
	newDb.run(
		query
	);
	
	query = "delete from tb_24cal";
	
	newDb.run(
		query
	);
	
	console.log("delete completed!!");
}

// tb_crops 테이블 insert
function insertCrop(crop) {
	var query =
		"insert into tb_crops ( " +
		"       cropId, name, ordering, period, difficulty, graph, icon, img " +
		") values ( " + 
		"       $cropId, $name, $ordering, $period, $difficulty, $graph, $icon, $img " +
		")";
		
	newDb.run(
		query,
		{
			$cropId : crop.cropId,
			$name : crop.name,
			$ordering : crop.ordering,
			$period : crop.period,
			$difficulty : crop.difficulty,
			$graph : crop.graph,
			$icon : crop.icon,
			$img : crop.img
		}
	);
}

// tb_24cal 테이블 insert
function insert24Cal(cal) {
	var query =
		"insert into tb_24cal ( " +
		"       no, name, date" +
		") values ( " + 
		"       $no, $name, $date" +
		")";
		
	newDb.run(
		query,
		{
			$no : cal.no,
			$name : cal.name,
			$date : cal.date
		}
	);
}

// tb_icons 테이블 insert
function insertIcon(icon) {
	var query =
		"insert into tb_icons ( " +
		"       iconId, title, url" +
		") values ( " + 
		"       $iconId, $title, $url" +
		")";
		
	newDb.run(
		query,
		{
			$iconId : icon.iconId,
			$title : icon.title,
			$url : icon.url
		}
	);
}

// tb_missions 테이블 insert
function insertMission(mission) {
	var query =
		"insert into tb_missions ( " +
		"       cropId, title, day, icon, guide" +
		") values ( " + 
		"       $cropId, $title, $day, $icon, $guide" +
		")";
		
	newDb.run(
		query,
		{
			$cropId : mission.cropId,
			$title : mission.title,
			$day : mission.day,
			$icon : mission.icon,
			$guide : mission.guide
		}
	);
}

// old icons -> new icons
function migrationIcons() {

	oldDb.each("select * from tb_icons", function(err, row) {
		var icon = {
			iconId : row.iconId,
			title : row.title,
			url : row.url
		};

		// insert newDb
		insertIcon(icon);
		
	}, function(){
		console.log("success migration Icons!!");
	});
	
}

// old crops -> new crops
function migrationCrops() {

	oldDb.each("select * from tb_crops", function(err, row) {
		var crop = {
			cropId : row.cropId,
			name : row.name,
			ordering : row.ordering,
			period : row.period,
			difficulty : row.difficulty,
			graph : row.graph,
			icon : row.icon,
			img : row.img
		};

		// insert newDb
		insertCrop(crop);
		
	}, function(){
		console.log("success migration Crops!!");
	});
	
}

// old 24cal -> new 24cal
function migration24Cal() {

	oldDb.each("select * from tb_24cal", function(err, row) {
		var cal = {
			no : row.no,
			name : row.name,
			date : row.date
		};

		// insert newDb
		insert24Cal(cal);
		
	}, function(){
		console.log("success migration 24cal!!");
	});
	
}

// old missions -> new missions
function migrationMissions() {

    var query = 
        "select a.cropId, b.title, a.day, c.url icon, b.guide " + 
        "  from tb_mission_by_crop a, " +
        "       tb_missions b, " +
        "       tb_icons c " +
        " where a.missionId = b.missionId " +
        "   and a.iconId = c.iconId";

	oldDb.each(query, function(err, row) {
		var mission = {
			cropId : row.cropId,
			title : row.title,
			day : row.day,
			icon : row.icon,
			guide : row.guide
		};

		// insert newDb
		insertMission(mission);
		
	}, function(){
		console.log("success migration missions!!");
	});
	
}

init();	// 새로운 DB 초기화

migrationIcons();

migrationCrops();

migration24Cal();

migrationMissions();