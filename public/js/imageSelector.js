var App = {
	Models : {},
	Views : {},
	start : function(type){
		var MainView = new App.Views.MainView({type: type});
	
	}
};

(function(){
	var selectImage, selectSrc;

	App.Views.MainView = Backbone.View.extend({
		el: $("#wrapper"),
		initialize: function(opt) {
			this.type = opt.type;
	  	},

		events: {
			"click li" : "handleSelect",
	    	"click .close" : "handleClose"
	  	},

	  	handleSelect: function(e){
	  		var $el = $(e.currentTarget), elImg;

	  		this.$el.find(".on").removeClass("on");
	  		$el.addClass("on");
	  		elImg = $el.find("img")[0];

	  		selectImage = elImg.className;
	  		selectSrc = elImg.src;

	  		console.log(selectImage, selectSrc);
	  	},

		handleClose: function(e) {
			top.opener.App.selectImage(this.type, selectImage, selectSrc);
			window.close();
		}
	});
})();