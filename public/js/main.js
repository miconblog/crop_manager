(function(){

	

    var View = Backbone.View.extend({
        el : $("#wrap"),
        events : {
            "click ._tab" : "handleTab"
        },

        handleTab : function(e){
            var tabName = e.currentTarget.className.split("tb_")[1];

            console.log(tabName);

            this.$el.find("._tab").removeClass("active");
            this.$el.find(".tab").removeClass("active");
            this.$el.find("." + tabName).addClass("active");
            this.$el.find(".tb_" + tabName).addClass("active");
        }

    });

    new View();



})();