var App = {
	Models : {},
	Views : {},
	start : function(cropInfo, missions){

		console.log(cropInfo.graph)

		cropInfo.missions = missions;
		this.basicModel = new App.Models.BasicModel(cropInfo);
		this.view = new App.Views.MainView({model: this.basicModel});
	},
	selectImage : function(type, str, src){
		switch(type){
			case "cropIcon":
			this.view.updateBasicCropIcon(str, src);
			break;
			case "cropImg":
			this.view.updateBasicCropImage(str, src);				
			break;
			case "mission":
			this.view.updateMissionIcon(str, src);
			break;
		}
	}
};

App.Models.BasicModel = Backbone.Model.extend({
	idAttribute: "cropId",
	urlRoot : "/crops",
	defaults : {
		current_select : null,
		graph : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	},
	url : function(){
		return this.urlRoot + "/" + this.get("cropId");
	},
	initialize: function() {
    	this.set("graph_tmp", this.get("graph"));
    	console.log("init model: ", this.toJSON());
  	}
});

App.Views.MainView = Backbone.View.extend({

	el: $("#wrap"),
	initialize: function() {
    	this.model.on("change:name", function(model, name){
    		console.log("name change", name);

    	});

    	this.model.on("change:icon", function(model, icon){
    		console.log("icon change", icon, this);

    	}.bind(this));

    	this.model.on("change:current_select", function(model, name){
			var $el = this.$el.find(".current_select");
			$el.attr("class", "current_select");

    		if( name ){
    			$el.text("STEP2에서 색칠하세요!");
    			this.$el.find(".current_select").addClass(name);
    		}else{
    			$el.text("왼쪽 단계를 선택하세요!");
    		}

    	}.bind(this));
  	},

  	renderBasicInfo : function(){
		var fields = this.$el.find("#basic tbody input");

		_.each(fields, function(field){
			field.value = this.model.get(field.name);
		}.bind(this));

  	},

  	renderGraph : function(){
  		var graph = this.model.get("graph");
  		var blocks = this.$el.find("#steps .block_month .block");

  		graph.forEach(function(v, i){
  			blocks[i].className = "block color" + v + " index_" + i;
  		});
  		this.model.set("current_select", null);
  		this.model.set("graph_tmp", graph);
  	},

  	updateBasicCropImage : function(value, src){
  		var fields = this.$el.find("#basic tbody input");
		var field = _.filter(fields, function(field){
			return field.name === "img";
		});
		field[0].value = value;
		this.$el.find("#cropImage").attr("src", src);
  	},

  	updateBasicCropIcon : function(value, src){
		var fields = this.$el.find("#basic tbody input");
		var field = _.filter(fields, function(field){
			return field.name === "icon";
		});
		field[0].value = value;
		this.$el.find("#cropIcon").attr("src", src);
  	},

  	updateMissionIcon : function(value, src){
  		console.log(value, src);
  	},

	events: {
        "click ._tab"               : "handleTab",
		"click ._image" 			: "handleImageSelector",
		"click #basic .reset" 		: "handleReset",
    	"click .save" 				: "handleSave",
    	"click #steps .select_step" : "handleSelectStep1",
    	"click #steps .block_month .block" : "handleSelectStep2",
    	"click #steps .reset" 		: "handleStep2Reset",
    	"click #missions .deleteRow": "handleDeleteMission",
    	"click #missions .add" 		: "handleAddMission"
   	},

    handleTab : function(e){
        var tabName = e.currentTarget.className.split("tb_")[1];

        console.log(tabName);

        this.$el.find("._tab").removeClass("active");
        this.$el.find(".tab").removeClass("active");
        this.$el.find("." + tabName).addClass("active");
        this.$el.find(".tb_" + tabName).addClass("active");
    },

  	handleImageSelector: function(e){
  		var options = "width=540, height=360, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no";
  		var url = "/kr/popup/imageSelector/";

  		if(e.currentTarget.className.indexOf("cropIcon") > -1){
  			url += "cropIcon";
  		}

  		if(e.currentTarget.className.indexOf("cropImg") > -1){
  			url += "cropImg";
  		}

  		if(e.currentTarget.className.indexOf("mission") > -1){
  			url += "mission";
  		}

  		window.open(url, "_blank", options);
  	},

  	handleReset: function(e) {
		this.renderBasicInfo();	
	},

	handleSave: function(e) {
		var fields = this.$el.find("#basic tbody input");
		var data = {};

		_.each(fields, function(field){
			data[field.name] = field.value;
		});
		data.graph = this.model.get("graph_tmp");
		
		//this.model.unset("current_select");
		//this.model.unset("graph_tmp");
		this.model.save(data, {
			silent: false,
			success : function(){
				alert("저장되었습니다.");
			}
		});
		console.log("update data: ", this.model.toJSON());
	},

	handleSelectStep1 : function(e){
		var name = e.currentTarget.className.match(/(color\d)/)[0];
		var prev = this.model.get("current_select");

		if( name === prev ){
			this.model.set("current_select", null);			
		}else{
			this.model.set("current_select", name);
		}
	},

	handleSelectStep2 : function(e){
		var $el = $(e.currentTarget);
		var select_step = this.model.get("current_select");
		var index = $el.attr("class").match(/index_(\d+)/)[1] - 0;
		var graph = this.model.get("graph");

		if( select_step ){
			graph[index] = select_step.replace("color", "") - 0;
			$el.attr("class", "block index_"+index);
			$el.addClass(select_step);
			this.model.set("graph_tmp", graph);	// 임시로 저장한다.	
		}
	},

	handleStep2Reset: function(e) {
		this.renderGraph();	
	},

	handleDeleteMission: function(e){
		var el = e.currentTarget;

		$(el).parents(".item").remove();

	},

	handleAddMission: function(e){
		$("#layer").modal("show");
	}

});