Node Server Guide
=======

### How to Install
```
 %> git clone git@bitbucket.org:miconblog/crop_manager.git
 %> cd crop_manager
 %> npm install 
```

### Running Node Server
```
 %> node server.js
```

### Running With nodemon
```
 %> npm install -g nodemon  
 %> nodemon server.js
```

### Run with Browser
```
 http://localhost:3000/
```

### Trouble Shooting 
 - make sure installed recent **git version(1.8.5)** on your CentOS 6+
 - need some packages
  - yum install perl-ExtUtils-MakeMaker
  - yum install gettext-devel
 - source from
 ```
   %> wget https://www.kernel.org/pub/software/scm/git/git-1.8.5.tar.gz
 ```
 - unzip & compile & install
 ```  
  %> tar -xvf git-1.8.5.tar.gz  
  %> cd git-1.8.5  
  %> ./configure --prefix=/usr/local (install location)  
  %> make  
  %> make install  
 ```

 - need RSA key
  - see here ([generating-ssh-keys][1])
  
[1]: https://help.github.com/articles/generating-ssh-keys

### How to Depoly
 - make sure login account: **node**  
 ```
  %> ssh node@dong9.org
  %> cd /app/crop_manager
  %> git pull
 ```
 - start/restart forever demon
 ```
  %> forever start sever.js -l /logs/crop_manager/forever.log -a
  %> forever restartall
 ```
